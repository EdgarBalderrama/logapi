# Log service
This application would be listening to a queue in  RabbitMQ and would store everything that comes from the queue into a postgres12 database.
The producers are:
- [Product microservice](https://gitlab.com/EdgarBalderrama/productapi)
- [Order microservice](https://gitlab.com/EdgarBalderrama/orderapi)
- [Company microservice](https://gitlab.com/EdgarBalderrama/companyapi)

Basic HTTP-type operations are contemplated.

Connection to RabbitMQ for logger, where this microservice has the role of consumer.

# Authors
- [Rodolfo Justiniano](https://gitlab.com/rodolfo.justiniano) as trainer
- [Edgar Balderrama](https://gitlab.com/EdgarBalderrama) as developer
