package com.milankas.training.domain.repository;

import com.milankas.training.domain.Log;

import java.util.List;

public interface LogRepository {

    Log save(Log log);
    List<Log> findAll(Log log);
}
