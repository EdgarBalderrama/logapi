package com.milankas.training.domain.exceptionhandler.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ApiErrorDetail {

    private String cause;
    private String message;
}

