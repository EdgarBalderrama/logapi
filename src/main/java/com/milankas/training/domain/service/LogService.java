package com.milankas.training.domain.service;

import com.milankas.training.domain.Log;
import com.milankas.training.domain.exceptionhandler.apiexceptions.BadRequestException;
import com.milankas.training.domain.repository.LogRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    private final LogRepository logRepository;

    public LogService(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public List<Log> findBy(String level, String service, String message) {
        Log log = new Log();
        log.setLevel(level);
        log.setService(service);
        log.setMessage(message);

        return logRepository.findAll(log);
    }

    public void save(Log log){
        logRepository.save(log);
    }

    public List<Log> getLogsWithDateInterval(List<Log> logs, String fromDateString, String toDateString) {
        LocalDateTime fromDateTime = (fromDateString != null)
                ? formatDateTime(fromDateString)
                : formatDateTime("01-01-2000 00:00:01");
        LocalDateTime toDateTime = (toDateString != null)
                ? formatDateTime(toDateString)
                : formatDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));

        if (fromDateTime.isAfter(toDateTime)) {
            throw new BadRequestException("from date time parameter needs to be before to date time parameter");
        }
        return filterLogs(logs, fromDateTime, toDateTime);
    }

    private List<Log> filterLogs(List<Log> logs, LocalDateTime fromDateTime, LocalDateTime toDateTime) {
        List<Log> returnLogs = new ArrayList<>();
        logs.forEach(log -> {
            String logDateString = log.getDateTime();
            LocalDateTime logDateTime = formatDateTime(logDateString);
            if (logDateTime.isBefore(toDateTime) && logDateTime.isAfter(fromDateTime)) {
                returnLogs.add(log);
            }
        });
        return returnLogs;
    }

    private LocalDateTime formatDateTime(String dateTime) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            return LocalDateTime.parse(dateTime, formatter);
        } catch (Exception e) {
            throw new BadRequestException("wrong date time format");
        }
    }
}
