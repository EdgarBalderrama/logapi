package com.milankas.training.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Log {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private UUID logId;
    private String level;
    private String dateTime;
    private String service;
    private String message;
}
