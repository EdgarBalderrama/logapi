package com.milankas.training.persistance.crud;

import com.milankas.training.persistance.entity.LogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LogCrudRepository extends JpaRepository<LogEntity, UUID> { }
