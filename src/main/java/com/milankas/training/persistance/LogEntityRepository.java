package com.milankas.training.persistance;

import com.milankas.training.domain.Log;
import com.milankas.training.domain.repository.LogRepository;
import com.milankas.training.persistance.crud.LogCrudRepository;
import com.milankas.training.persistance.entity.LogEntity;
import com.milankas.training.persistance.mapper.LogMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Repository
public class LogEntityRepository implements LogRepository {

    private LogCrudRepository logCrudRepository;
    private LogMapper logMapper;

    public LogEntityRepository(LogCrudRepository logCrudRepository, LogMapper logMapper) {
        this.logCrudRepository = logCrudRepository;
        this.logMapper = logMapper;
    }

    @Override
    public Log save(Log log) {
        LogEntity logEntity = logMapper.toLogEntity(log);
        return logMapper.toLog(logCrudRepository.save(logEntity));
    }

    @Override
    public List<Log> findAll(Log log) {
        LogEntity logEntity = logMapper.toLogEntity(log);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withMatcher("message", contains());

        Example<LogEntity> logEntityExample = Example.of(logEntity, matcher);
        List<LogEntity> logs = logCrudRepository.findAll(logEntityExample);

        return logMapper.toLogs(logs);
    }
}
