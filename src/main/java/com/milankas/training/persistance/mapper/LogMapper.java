package com.milankas.training.persistance.mapper;

import com.milankas.training.domain.Log;
import com.milankas.training.persistance.entity.LogEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LogMapper {

    @Mappings({
            @Mapping(source = "id", target = "logId"),
    })
    Log toLog(LogEntity logEntity);
    List<Log> toLogs(List<LogEntity> logEntities);

    @InheritInverseConfiguration
    LogEntity toLogEntity(Log log);
}
