package com.milankas.training.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "logs")
@Getter
@Setter
public class LogEntity {

    @Id
    @GeneratedValue
    private UUID id;
    private String level;
    private String dateTime;
    private String service;

    @Column(columnDefinition="TEXT")
    private String message;
}
