package com.milankas.training;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Log API", version = "1.0.0", description = "Log Microservice"))
public class LogapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogapiApplication.class, args);
	}

}
