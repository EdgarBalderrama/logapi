package com.milankas.training.web.controller;

import com.milankas.training.domain.Log;
import com.milankas.training.domain.service.LogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/logs")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping
    public List<Log> getByParams(@RequestParam(value = "level", required = false) String level,
                                 @RequestParam(value = "service", required = false) String service,
                                 @RequestParam(value = "message", required = false) String message,
                                 @RequestParam(value = "fromDateTime", required = false) String fromDateTime,
                                 @RequestParam(value = "toDateTime", required = false) String toDateTime) {
        List<Log> logs = logService.findBy(level, service, message);
        return logService.getLogsWithDateInterval(logs, fromDateTime, toDateTime);
    }
}
