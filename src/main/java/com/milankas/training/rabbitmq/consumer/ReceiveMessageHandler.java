package com.milankas.training.rabbitmq.consumer;

import com.milankas.training.domain.Log;
import com.milankas.training.domain.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@Slf4j
public class ReceiveMessageHandler {

    public static final int BEGIN_INDEX = 6;
    public static final int ERROR_INDEX = 13;
    LogService logService;

    public ReceiveMessageHandler(LogService logService) {
        this.logService = logService;
    }

    public void handleMessage(String messageBody){
        Log log = new Log();
        setDateTime(log);
        setService(messageBody, log);
        setLevel(messageBody, log);

        logService.save(log);
    }

    private void setDateTime(Log log) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDate = currentDateTime.format(dateTimeFormatter);
        log.setDateTime(formattedDate);
    }

    private void setLevel(String messageBody, Log log) {
        if (messageBody.substring(BEGIN_INDEX).startsWith("Error")){
            log.setLevel("Error");
            log.setMessage(messageBody.substring(ERROR_INDEX));
        }
        else {
            log.setMessage(messageBody.substring(BEGIN_INDEX));
            log.setLevel("Info");
        }
    }

    private void setService(String messageBody, Log log) {
        if (messageBody.startsWith("ProApi"))
            log.setService("Product API");
        else if (messageBody.startsWith("OrdApi"))
            log.setService("Order API");
        else if (messageBody.startsWith("ComApi"))
            log.setService("Company API");
        else
            log.setService("Milankas Api");
    }
}
